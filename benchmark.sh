#!/bin/bash

DIR=`pwd`
DATA=`pwd`"/data"
SRC=`pwd`"/my_benchmark"

for CIPHER in "ascon" "gift" "grain" "isap" "photon" "sparkle" "tinyjambu" "romulus" "elephant" "xoodyak"; do
	clear
	echo "$CIPHER"
	
	cd "$SRC""/""$CIPHER"

	# Compilation.
	./compile.sh
	
	# Benchmarking.
	./benchmark
done
