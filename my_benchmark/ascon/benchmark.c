// Author: André Gomes
// Email: gomesa@vt.edu

#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "amd64rdpmc.c"
#include "crypto_aead.h"
#include "api.h"

#define KAT_SUCCESS          0
#define KAT_FILE_OPEN_ERROR -1
#define KAT_DATA_ERROR      -3
#define KAT_CRYPTO_FAILURE  -4

#define MAX_FILE_NAME				256
#define MAX_MESSAGE_LENGTH			32
#define MAX_ASSOCIATED_DATA_LENGTH	32

// Benchmark parameters.
#define NUM_TESTS	1E7
#define MSG_LENGTH 	32
#define AD_LENGTH   0

void unhexlify(unsigned char *buff, char *str, int slen);
unsigned char unhexlify_char(char c);
void gen_rand_buffer(unsigned char *buffer, unsigned long long numbytes);


int main() {
	// const unsigned char *msg, *nonce, *key;
	unsigned char 		msg[MSG_LENGTH];
	unsigned char 		nonce[CRYPTO_NPUBBYTES];
	unsigned char 		key[CRYPTO_KEYBYTES];
	unsigned char		ad[MAX_ASSOCIATED_DATA_LENGTH];
	unsigned char		ct[MAX_MESSAGE_LENGTH + CRYPTO_ABYTES];
	int  				encrypt_ret, decrypt_ret;
	unsigned long long  clen, mlen;
	long long 			cycles;
	char 				fname_encrypt[1000], fname_decrypt[1000];
	FILE				*fpe, *fpd;

	srand(0);

	sprintf(fname_encrypt, "../../data/runtime/ascon_encryption.log");
	sprintf(fname_decrypt, "../../data/runtime/ascon_decryption.log");
	fpe = fopen(fname_encrypt, "w");
	fpd = fopen(fname_decrypt, "w");

	// Benchmark.
	for (long long i=0; i<NUM_TESTS; i++){
		// Generate random inputs.
		// First encryption/decryption is discarded to avoid timing initializing operations (e.g., memory allocation).
		gen_rand_buffer(msg, MSG_LENGTH); // message
		gen_rand_buffer(nonce, CRYPTO_NPUBBYTES); // nonce
		gen_rand_buffer(key, CRYPTO_KEYBYTES); // key

		// Encryption.
		cycles = cpucycles_amd64rdpmc();
		encrypt_ret = crypto_aead_encrypt(ct, &clen, msg, MSG_LENGTH, ad, AD_LENGTH, NULL, nonce, key);
		cycles = cpucycles_amd64rdpmc() - cycles;
		if (encrypt_ret == KAT_SUCCESS) {
			fprintf(fpe, "%lli\n", cycles);
		} else {
			fprintf(fpe, "Error! Decryption failed.\n");
		}

		// Decryption.
		cycles = cpucycles_amd64rdpmc();
		decrypt_ret = crypto_aead_decrypt(msg, &mlen, NULL, ct, clen, ad, AD_LENGTH, nonce, key);
		cycles = cpucycles_amd64rdpmc() - cycles;
		if (decrypt_ret == KAT_SUCCESS) {
			fprintf(fpd, "%lli\n", cycles);
		} else {
			fprintf(fpd, "Error! Decryption failed.\n");
		}
	}

	return 0;
}

void unhexlify(unsigned char *buff, char *str, int slen) {
	unsigned char c;
	
	for (int i = 0; i < slen/2; i++) {
		c = unhexlify_char(str[2*i]);
		buff[i] = c << 4;
		c = unhexlify_char(str[2*i+1]);
		buff[i] += c;
	}
}

unsigned char unhexlify_char(char c) {
	// Do not add "break," so run time is constant.
	unsigned char b;
	switch (c){
		case '0':
			b = 0x00;
		case '1':
			b = 0x01;
		case '2':
			b = 0x02;
		case '3':
			b = 0x03;
		case '4':
			b = 0x04;
		case '5':
			b = 0x05;
		case '6':
			b = 0x06;
		case '7':
			b = 0x07;
		case '8':
			b = 0x08;
		case '9':
			b = 0x09;
		case 'a':
			b = 0x0a;
		case 'b':
			b = 0x0b;
		case 'c':
			b = 0x0c;
		case 'e':
			b = 0x0e;
		case 'f':
			b = 0x0f;
	}
	return b;
}

void gen_rand_buffer(unsigned char *buffer, unsigned long long numbytes)
{
	for (unsigned long long i = 0; i < numbytes; i++)
		buffer[i] = rand()%256;
}