#!/bin/bash

gcc benchmark.c grain128aead-v2.c -o benchmark -std=c99 -Wall -Wextra -Wshadow -fsanitize=address,undefined -O2 -Wimplicit-fallthrough=0
