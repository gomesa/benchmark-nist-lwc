#!/bin/bash

gcc benchmark.c crypto_aead.c isap.c KeccakP-400-reference.c -o benchmark -std=c99 -Wall -Wextra -Wshadow -fsanitize=address,undefined -O2 -Wimplicit-fallthrough=0
