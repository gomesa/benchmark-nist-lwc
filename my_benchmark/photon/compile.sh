#!/bin/bash

gcc benchmark.c encrypt.c photon.c -o benchmark -std=c99 -Wall -Wextra -Wshadow -fsanitize=address,undefined -O2 -Wimplicit-fallthrough=0
