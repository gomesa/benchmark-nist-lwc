// Author: André Gomes
// Email: gomesa@vt.edu

#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "crypto_aead.h"
#include "api.h"
// #include "encrypt.c"
// #include "aead.c"

#define KAT_SUCCESS          0
#define KAT_FILE_OPEN_ERROR -1
#define KAT_DATA_ERROR      -3
#define KAT_CRYPTO_FAILURE  -4

#define MAX_FILE_NAME				256
#define MAX_MESSAGE_LENGTH			32
#define MAX_ASSOCIATED_DATA_LENGTH	32

// Benchmark parameters.
#define RANDOM_SEED	0
#define NUM_TESTS	1E3
#define MSG_LENGTH 	32
#define AD_LENGTH   0

void unhexlify(unsigned char *buff, char *str, int slen);
unsigned char unhexlify_char(char c);


int main(int argc, char **argv) {
	// const unsigned char *msg, *nonce, *key;
	unsigned char 		msg[MSG_LENGTH];
	unsigned char 		nonce[CRYPTO_NPUBBYTES];
	unsigned char 		key[CRYPTO_KEYBYTES];
	unsigned char		ad[MAX_ASSOCIATED_DATA_LENGTH];
	unsigned char		ct[MAX_MESSAGE_LENGTH + CRYPTO_ABYTES];
	int  				encrypt_ret;
	unsigned long long  clen;

	if (argc != 4) {
		printf("Error: inputs must be message, nonce, and key.");
	}

	unhexlify(msg, argv[1], (int) strlen(argv[1]));
	unhexlify(nonce, argv[2], (int) strlen(argv[2]));
	unhexlify(key, argv[3], (int) strlen(argv[3]));

	// for (int i = 0; i < (int) strlen(argv[3]); i++) {
	// 	printf("%c", argv[3][i]);
	// }
	// printf("\n");

	// for (int i = 0; i < CRYPTO_KEYBYTES; i++) {
	// 	printf("%0.2x", key[i]);
	// }
	// printf("\n\n");

	encrypt_ret = crypto_aead_encrypt(ct, &clen, msg, MSG_LENGTH, ad, AD_LENGTH, NULL, nonce, key);
	
	if (encrypt_ret != KAT_SUCCESS) {
		printf("ERROR: encryption failed.\n");
	}

	return 0;
}

void unhexlify(unsigned char *buff, char *str, int slen) {
	unsigned char c;
	
	for (int i = 0; i < slen/2; i++) {
		c = unhexlify_char(str[2*i]);
		buff[i] = c << 4;
		c = unhexlify_char(str[2*i+1]);
		buff[i] += c;
	}
}

unsigned char unhexlify_char(char c) {
	// Do not add "break," so run time is constant.
	unsigned char b;
	switch (c){
		case '0':
			b = 0x00;
		case '1':
			b = 0x01;
		case '2':
			b = 0x02;
		case '3':
			b = 0x03;
		case '4':
			b = 0x04;
		case '5':
			b = 0x05;
		case '6':
			b = 0x06;
		case '7':
			b = 0x07;
		case '8':
			b = 0x08;
		case '9':
			b = 0x09;
		case 'a':
			b = 0x0a;
		case 'b':
			b = 0x0b;
		case 'c':
			b = 0x0c;
		case 'e':
			b = 0x0e;
		case 'f':
			b = 0x0f;
	}
	return b;
}