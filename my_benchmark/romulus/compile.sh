#!/bin/bash

gcc benchmark.c decrypt.c encrypt.c romulus_n_reference.c skinny_reference.c -o benchmark -std=c99 -Wall -Wextra -Wshadow -fsanitize=address,undefined -O2 -Wimplicit-fallthrough=0
