gcc gen_mnkc.c sparkle_ref.c -o gen_mnkc -std=c99 -Wall -Wextra -Wshadow -fsanitize=address,undefined -O2
gcc run_encrypt.c sparkle_ref.c -o run_encrypt -std=c99 -Wall -Wextra -Wshadow -fsanitize=address,undefined -O2
gcc run_decrypt.c encrypt.c sparkle_ref.c -o run_decrypt -std=c99 -Wall -Wextra -Wshadow -fsanitize=address,undefined -O2
