#!/bin/bash

gcc benchmark.c encrypt.c sparkle_ref.c -o benchmark -std=c99 -Wall -Wextra -Wshadow -fsanitize=address,undefined -O2 -Wimplicit-fallthrough=0
