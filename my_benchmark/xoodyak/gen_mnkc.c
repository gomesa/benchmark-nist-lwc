// Author: André Gomes
// Email: gomesa@vt.edu

#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include "crypto_aead.h"
#include "api.h"

#define KAT_SUCCESS          0
#define KAT_FILE_OPEN_ERROR -1
#define KAT_DATA_ERROR      -3
#define KAT_CRYPTO_FAILURE  -4

#define MAX_FILE_NAME				256
#define MAX_MESSAGE_LENGTH			32
#define MAX_ASSOCIATED_DATA_LENGTH	32

// Benchmark parameters.
#define RANDOM_SEED	0
#define NUM_TESTS	1E6
#define MSG_LENGTH 	32
#define AD_LENGTH   0

void gen_rand_buffer(unsigned char *buffer, unsigned long long numbytes);

int main() {
    FILE                *fp;
    char                fname[1000];
	unsigned char       key[CRYPTO_KEYBYTES];
	unsigned char		nonce[CRYPTO_NPUBBYTES];
	unsigned char       msg[MAX_MESSAGE_LENGTH];
	unsigned char		ad[MAX_ASSOCIATED_DATA_LENGTH];
	unsigned char		ct[MAX_MESSAGE_LENGTH + CRYPTO_ABYTES];
	unsigned long long  clen;
	int  				encrypt_ret;

    srand(0);

    sprintf(fname, "../../data/inputs/xoodyak_mnkc.txt");
    fp = fopen(fname, "wb");

    for (int i = 0; i < NUM_TESTS; i++) {
        // Message.
        gen_rand_buffer(msg, MSG_LENGTH);
        for (int j = 0; j < MSG_LENGTH; j++)
            fprintf(fp, "%.2x", msg[j]);
        fprintf(fp, " ");

        // Nonce.
        gen_rand_buffer(nonce, CRYPTO_NPUBBYTES);
        for (int j = 0; j < CRYPTO_NPUBBYTES; j++)
            fprintf(fp, "%.2x", nonce[j]);
        fprintf(fp, " ");

        // Key.
        gen_rand_buffer(key, CRYPTO_KEYBYTES);
        for (int j = 0; j < CRYPTO_KEYBYTES; j++){
            fprintf(fp, "%.2x", key[j]);}
        fprintf(fp, " ");

        // Ciphertext.
        encrypt_ret = crypto_aead_encrypt(ct, &clen, msg, MSG_LENGTH, ad, AD_LENGTH, NULL, nonce, key);
        if (encrypt_ret != KAT_SUCCESS) {
			printf("ERROR: encryption failed.\n");
			return -1;
		}
        for (int j = 0; j < (int)clen; j++)
            fprintf(fp, "%.2x", ct[j]);
        fprintf(fp, "\n");
    }

	return 0;
}

void gen_rand_buffer(unsigned char *buffer, unsigned long long numbytes)
{
	for (unsigned long long i = 0; i < numbytes; i++)
		buffer[i] = rand()%256;
}
